import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { getStorage } from 'src/app/utils/storage.utils';
import { LoginService } from '../../../services/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
})
export class LoginForm implements OnInit {
  @Output() success: EventEmitter<void> = new EventEmitter();

  public trainerLoginForm: FormGroup = new FormGroup({
    trainerName: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
    ]),
  });

  constructor(
    private readonly loginService: LoginService,
  ) {}

  ngOnInit(): void {
    const existingTrainer = getStorage<any>('pokemon-trainer');
    if (existingTrainer !== null) {
      this.success.emit();
    }
  }

  public get trainerName(): AbstractControl {
    return this.trainerLoginForm.get('trainerName');
  }

  get loading(): boolean {
    return this.loginService.loading;
  }

  get error(): string {
    return this.loginService.error;
  }

  onLogin() {
    const { trainerName } = this.trainerLoginForm.value;
    this.loginService
      .login(trainerName)
      .subscribe(
        this.handleSuccessfulLogin.bind(this),
        this.handleLoginError.bind(this)
      );
  }

  handleSuccessfulLogin(trainer): void {
    this.success.emit();
  }

  handleLoginError(error): void {
    if (this.loginService.errorCount >= 3) {
    }
    console.log(error);
  }
}
