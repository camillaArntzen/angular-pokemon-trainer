import { Component } from '@angular/core';

@Component({
  selector: 'app-container',
  template: ` <div class="container">
    <ng-content></ng-content>
  </div>`,

  styles: [
    `
      .container {
        width: 100%;
        margin: 0 auto;
      }
    `,
  ],
})
export class AppContainerComponent {}
