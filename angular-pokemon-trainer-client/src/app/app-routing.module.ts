import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SessionGuard } from './guards/session.guard';
//Components
import { LoginPage } from './pages/login/login.page';
import { PageNotFound } from './pages/page-not-found/page-not-found.page';
import { PokemonCatalogPage } from './pages/pokemon-catalog-page/pokemon-catalog.page';
import { PokemonTrainerPage } from './pages/pokemon-trainer-page/pokemon-trainer.page';
import { PokemonDetailPage } from './pages/pokemon-detail-page/pokemon-detail.page';

//routes
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login',
  },
  {
    path: 'login',
    component: LoginPage,
  },
  {
    path: 'trainer',
    component: PokemonTrainerPage,
    canActivate: [SessionGuard],
  },

  {
    path: 'pokemon-catalog',
    component: PokemonCatalogPage,
    canActivate: [SessionGuard],
  },
  {
    path: 'pokemon/:name',
    component: PokemonDetailPage,
    canActivate: [SessionGuard],
  },

  {
    path: '**',
    component: PageNotFound,
  }, //wildcard route for 404
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
