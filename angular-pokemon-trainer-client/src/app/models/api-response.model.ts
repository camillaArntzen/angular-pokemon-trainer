export interface ApiResponse {
  success: boolean;
  results: any;
}
