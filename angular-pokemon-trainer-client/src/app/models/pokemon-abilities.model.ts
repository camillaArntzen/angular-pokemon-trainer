export interface PokemonAbilities {
  ability: {
    name: string;
    url: string;
  };
}
