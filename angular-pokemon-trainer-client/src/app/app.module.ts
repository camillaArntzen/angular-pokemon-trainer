import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
//components
import { LoginForm } from './components/forms/login-form/login-form.component';
import { PageNotFound } from './pages/page-not-found/page-not-found.page';
import { AppContainerComponent } from './components/container/container.component';
import { LoginPage } from './pages/login/login.page';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PokemonCatalogPage } from './pages/pokemon-catalog-page/pokemon-catalog.page';
import { PokemonTrainerPage } from './pages/pokemon-trainer-page/pokemon-trainer.page';
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { PokemonDetailPage } from './pages/pokemon-detail-page/pokemon-detail.page';

@NgModule({
  declarations: [
    AppComponent,
    LoginForm,
    LoginPage,
    PokemonCatalogPage,
    PokemonTrainerPage,
    NavbarComponent,
    PageNotFound,
    AppContainerComponent,
    PokemonComponent,
    PokemonDetailPage,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
