import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { environment } from 'src/environments/environment';
import { getStorage } from '../utils/storage.utils';

const { trainerAPI } = environment;

@Injectable({
  providedIn: 'root',
})

export class TrainerService {

  public pokemons: Pokemon[] = [];
  public error: string = '';
  private trainer: Trainer;

  constructor(private readonly http: HttpClient) {}

  public getTrainerName() {
    this.getTrainer();
  }

  get trainerName(): string {
    return getStorage('pokemon-trainer').name;
  }

  //get trainer from db
  public getTrainer() {
    const trainerName = this.trainerName;
    this.http.get(`${trainerAPI}/trainers?name=${trainerName}`).subscribe(
      (data: any) => {
        this.trainer = data[0];
        this.pokemons = data[0].pokemons;
      },
      (error) => {
        console.log('Error: ', error);
      }
    );
  }

  //save pokemons to db
  public storePokemon(pokemon) {
      this.pokemons.push(pokemon);
      this.http
        .patch(`${trainerAPI}/trainers/${this.trainer.id}`, {
            pokemons: this.pokemons,
      })
      .subscribe(
        (response) => {
          console.log('Success! ', response);
        },
        (error) => {
          console.log('Error: ', error);
        }
      );
  }
}
