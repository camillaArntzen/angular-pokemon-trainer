import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import {  mergeMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { logoutUser, setStorage } from '../utils/storage.utils';
import { Router } from '@angular/router';
import { SessionService } from './session.service';
import { AppRoutes } from 'src/enums/app-routes.enum';
import { PokemonService } from './pokemon.service';

//api URL from environments
const { trainerAPI } = environment;

@Injectable({
  providedIn: 'root',
})

export class LoginService {

  public loading: boolean = false;
  public error: string = '';
  public errorCount: number = 0;

  constructor(
    private readonly http: HttpClient,
    private sessionService: SessionService,
    private pokemonService: PokemonService,
    private router: Router
  ) {}

  private setTrainer(trainer: any) {
    setStorage('pokemon-trainer', trainer);
  }
  
  //login or register trainer
  login(trainerName: string): Observable<any> {
        const login$ = this.http.get(`${trainerAPI}/trainers?name=${trainerName}`);
        const register$ = this.http.post(trainerAPI + '/trainers', {
            name: trainerName,
            pokemons: [],
        });
        
        return login$.pipe(
            mergeMap((loginResponse: any[]) => {
                const trainer = loginResponse.pop();

        if (!trainer) {
          return register$;
        } else {
          return of(trainer);
        }
      }),
      tap((trainer) => {
        this.setTrainer(trainer);
      })
    );
  }

  //logout trainer
  logout() {
    console.log(this.sessionService.isLoggedIn());
    this.pokemonService.offset = 0;
    logoutUser();
    //redirects to login page
    this.router.navigateByUrl(AppRoutes.LOGIN);
  }

  get id(): string {
    return this.isLoggedIn ? this.sessionService.trainerId : '';
  }

  get isLoggedIn(): boolean {
    return this.sessionService.trainerId !== '';
  }
}
