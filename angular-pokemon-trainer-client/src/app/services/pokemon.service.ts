import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Pokemon } from '../models/pokemon.model';
import { async, Observable, throwError } from 'rxjs';
import { catchError, tap, map, shareReplay } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { PokemonResponse } from '../models/pokemon-response.model';


@Injectable({
  providedIn: 'root',
})

export class PokemonService {
    
    public pokemons: Pokemon[] = [];
    public error: string = '';
    private _offset = 0;

  public get offset() {
    return this._offset;
  }

  public set offset(value) {
    this._offset = value;
  }

  constructor(private readonly http: HttpClient) {  }

  public next() {
    if (this.offset >= 1100) {
    } else {
      this.offset += 24;
      this.fetchPokemon();
    }
  }

  public prev() {
    if (this.offset === 0) {
      return;
    }
    this.offset -= 24;
    this.fetchPokemon();
  }

  fetchPokemon(): void {
    this.http
      .get<PokemonResponse>(
        `${environment.basePokemonAPIUrl}${environment.pokeLimit}&offset=${this.offset}`
      )
      .pipe(shareReplay(1))
      .pipe(
        map((response: PokemonResponse) => {
          return response.results.map((pokemon: Pokemon) => ({
            ...pokemon,
            ...this.getIdAndImage(pokemon.url),
          }));
        })
      )
      .subscribe(
        (pokemons: Pokemon[]) => {
          pokemons.forEach((pokemon) => {
            this.getPokemonDetails(pokemon.url).subscribe((response: any) => {
                pokemon.height = response.height;
                pokemon.weight = response.weight;
                pokemon.types = response.types;
                pokemon.abilities = response.abilities;
                pokemon.stats = response.stats;
                pokemon.moves = this.getMoves(response.moves);
                pokemon.base_experience = response.base_experience;
              
            });
          });

          this.pokemons = pokemons;
        },
        (errorResponse: HttpErrorResponse) => {
          this.error = errorResponse.message;
        }
      );
  }

  private getMoves(moves) {
    let movesList = [];
    moves.forEach((move) => {
      movesList.push(move.move.name);
    });
    return movesList;
  }

  private getPokemonDetails(url: string) {
    return this.http.get(url);
  }

  private getIdAndImage(url: string): any {
    const id = url.split('/').filter(Boolean).pop();
    return {
      id: Number(id),
      image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`,
    };
  }
}
