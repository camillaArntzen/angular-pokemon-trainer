import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
//import service
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-pokemon-catalog-page',
  templateUrl: './pokemon-catalog.page.html',
  styleUrls: ['./pokemon-catalog.page.css'],
})

export class PokemonCatalogPage {
  constructor(
    private readonly pokemonService: PokemonService,
    public router: Router
  ) {}

  get pokemons(): Pokemon[] {
    return this.pokemonService.pokemons;
  }

  ngOnInit(): void {
    this.pokemonService.fetchPokemon();
  }

  onNextClick(): void { this.pokemonService.next(); }

  onPrevClick(): void { this.pokemonService.prev(); }

  //navigate to the detail page
  goToDetails(clickedPokemon: Pokemon) {
    let navigationExtras: NavigationExtras = {
      state: {
        pokemon: clickedPokemon,
      },
    };
    this.router.navigate([`pokemon/${clickedPokemon.name}`], navigationExtras);
  }
}
