import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { AppRoutes } from 'src/enums/app-routes.enum';
//import service
import { TrainerService } from 'src/app/services/trainer.service';


@Component({
  selector: 'app-pokemon-detail-page',
  templateUrl: './pokemon-detail.page.html',
  styleUrls: ['./pokemon-detail.page.css'],
})
export class PokemonDetailPage {
  public pokemon: Pokemon;

  constructor(
    private readonly trainerService: TrainerService,
    public route: ActivatedRoute,
    public router: Router
  ) {
    {
      this.route.queryParams.subscribe((params) => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.pokemon = this.router.getCurrentNavigation().extras.state
            .pokemon as Pokemon;
        }
      });
    }
  }

  collectPokemon() {
    this.trainerService.storePokemon(this.pokemon);
    //redirect to trainer page
    this.router.navigateByUrl(AppRoutes.TRAINER);
  }

  ngOnInit(): void {}
}
