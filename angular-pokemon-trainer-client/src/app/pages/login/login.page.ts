import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/enums/app-routes.enum';

@Component({
  selector: 'app-login-page',
  templateUrl: 'login.page.html',
  styleUrls: [ './login.page.css' ],
})
export class LoginPage {
  constructor(private readonly router: Router) {}

  handleSuccessfulLogin(): void {
    this.router.navigateByUrl(AppRoutes.TRAINER);
  }
}
