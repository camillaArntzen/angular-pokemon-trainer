import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
//import service
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-pokemon-trainer-page',
  templateUrl: './pokemon-trainer.page.html',
  styleUrls: ['./pokemon-trainer.page.css'],
})

export class PokemonTrainerPage {
  error: string = '';

  constructor(
    private readonly trainerService: TrainerService,
    public router: Router
  ) {}

  get pokemons(): Pokemon[] {
    return this.trainerService.pokemons;
  }

  get trainerName() {
    return this.trainerService.trainerName;
  }

  ngOnInit() {
    this.trainerService.getTrainer();
  }

   //navigate to the detail page
   goToDetails(clickedPokemon: Pokemon) {
    let navigationExtras: NavigationExtras = {
      state: {
        pokemon: clickedPokemon,
      },
    };
    this.router.navigate([`pokemon/${clickedPokemon.name}`], navigationExtras);
  }
}
