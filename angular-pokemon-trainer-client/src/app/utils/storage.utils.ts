import { Trainer } from 'src/app/models/trainer.model';

export function setStorage<T>(key: string, value: T): void {
  console.log(value);
  const json = JSON.stringify(value);
  const encoded = btoa(json);
  localStorage.setItem(key, encoded);
}

export function getStorage<T>(key: string): Trainer {
  const storedValue = localStorage.getItem(key);
  if (!storedValue) {
    return null;
  }
  const decoded = atob(storedValue);
  return JSON.parse(decoded) as Trainer;
}

export function logoutUser() {
  localStorage.clear();
  sessionStorage.clear();
}
