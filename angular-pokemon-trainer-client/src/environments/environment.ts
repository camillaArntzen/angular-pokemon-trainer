export const environment = {
  production: false,
  //url to pokemon API
  basePokemonAPIUrl: 'https://pokeapi.co/api/v2/pokemon/',
  //limit set to 24
  pokeLimit: '?limit=24',
  //url to trainer API (json server)
  trainerAPI: 'http://localhost:5000',
};
