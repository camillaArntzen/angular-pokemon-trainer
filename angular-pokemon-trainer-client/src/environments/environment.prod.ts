export const environment = {
  production: true,
  basePokemonAPIUrl: 'https://pokeapi.co/api/v2/pokemon/',
  trainerAPI: 'https://trainer.pokemon.com',
};
