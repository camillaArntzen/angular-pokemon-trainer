export enum AppRoutes {
  LOGIN = '/login',
  POKEMON = '/pokemon-catalog',
  POKEMON_DETAIL = '/pokemon/:name',
  TRAINER = '/trainer',
  HOME = '/',
}
