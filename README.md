# Pokemon trainer
The fourth javascript assignment for the Noroff .NET upskill course winter 2021. This project is a Pokemon Trainer web app developed using the Angular Framework. 
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.2.

## Features

<p>
Users can log in to the application or register as a Pokemon trainer by entering their trainer name on the landing page. If the login is successful the user is redirected to the trainer page. On the trainer page users can view their Pokemon collection. On the Pokemon catalog page users can view available Pokemon. If a user clicks a Pokemon card the user will be redirected to the detail page for the clicked Pokemon. On the details page users can view detailed information about the selected Pokemon. The user can also collect the Pokemon by clicking on the "Collect"- button. When a user collects a Pokemon the Pokemon will be stored along with the trainer name in the database. The user can log out of the Pokemon application by clicking "Log out" in the top navigation bar. 
</p>

## Setup

<ul>
<li> Clone the repository</li>
<li> npm install </li>
<li> Run `npm start` for the server-project </li>
<li> Run `ng serve` for the client-project </li>
<li> Navigate to `http://localhost:4200/`.  </li>
<li> Start collecting Pokemon </li>
<li>(You can log in with the trainer name 'Test' for a quick start)</li>
</ul>

## Author
<ul>
<li>Camilla Arntzen</li>
</ul>

## Assignment requirements

 #### General requirements
<p>&#10004; Use the latest angular with the Angular CLI</p>
<p>&#10004; Use components to create "Root" or "Parent" components for pages. Create reusable pieces of UI </p>
<p>&#10004; Used the angular router to display parent components </p>
<p>&#10004; Used the Angular guard pattern</p>
<p>&#10004; Used services to share data and make HTTP requests </p>

#### Landing page
<p>&#10004; The user must be presented with a landing page where they can input their “Trainer name”.</p>
<p>&#10004; The Trainer name must be stored in the database solution.</p>
<p>&#10004; When the user returns to the web app, they must not be asked for their trainer name again, but
simply redirected to the Pokemon page.</p>
 
#### Trainer page
<p>&#10004; Users may not have access to this page unless they have entered a trainer name. </p>
<p>&#10004; The Pokemon catalogue must be a list of “card style” Pokemon presented to the user. The
Pokemon should have an image (sprite) and name displayed.  </p>
<p>&#10004; Each Pokémon card must be clickable and take the user to the Pokemon detail page. </p>

#### Pokemon catalog
<p>&#10004; User may not have access to this page unless they have entered a trainer name</p>
<p>&#10004; Pokemon catalog must be a list of "card style" Pokemon presented to the user. The Pokemon should have an image and a name.</p>
<p>&#10004; Each Pokemon card must be clickable and take the user to the Pokemon detail page </p>

#### Pokemon details
<p>&#10004; The detail page displays an image of the Pokemon along with its abilities, stats, height, weight and types. </p>
<p>&#10004; "Collect" button to collect the Pokemon. Stored in server.</p>
<p>&#10004; Detail page divided into the required sections. </p>

## Bugs 😢
<ul>
<li>Can collect several of the same Pokemon </li>
</ul>